package utility;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;

public class ListenerMap<K, V> extends HashMap<K, V> {

    public static final String PROP_PUT = "put";
    public static final String PROP_REMOVE = "remove";

    private PropertyChangeSupport propertySupport;

    public ListenerMap() {
        super();
        propertySupport = new PropertyChangeSupport(this);
    }

    @Override
    public V put(K k, V v) {
        V old = super.put(k, v);
        propertySupport.firePropertyChange(PROP_PUT, null, k);
        return old;
    }

    @Override
    public V remove(Object o) {
        if (o instanceof String) {
            V old = super.remove(o);
            propertySupport.firePropertyChange(PROP_REMOVE, o, null);
            return old;
        } else {
            return null;
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
}
