import classes.Server;

import java.io.IOException;

public class Main {

    private final static int maxNumOfClients = 10;
    private final static int maxNumOfSockets = 100;
    private final static int serverPort = 1234;

    public static void main(String[] args) throws IOException
    {
        Server server = new Server(maxNumOfClients, maxNumOfSockets, serverPort);

        server.run();
    }
}
