package classes;

import java.beans.PropertyChangeListener;

import interfaces.ClientHandlerInterface;
import interfaces.ServerInterface;
import utility.ListenerMap;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Server extends Thread implements ServerInterface {

    private ListenerMap<String, ClientHandlerInterface> activeUsers;
    private HashMap<String, PropertyChangeListener> listeners;
    private ServerSocket serverSocket;
    private int maxNumOfClients;
    private int maxNumOfOpenedSockets;
    private int openedSocketCount;

    public Server(int maxNumOfClients, int maxNumOfOpenedSockets, int port) throws IOException {
        this.maxNumOfClients = maxNumOfClients;
        this.maxNumOfOpenedSockets = maxNumOfOpenedSockets;
        this.openedSocketCount = 0;
        this.serverSocket = new ServerSocket(port);
        this.activeUsers = new ListenerMap<>();
        this.listeners = new HashMap<>();
        activeUsers.put("All", null);
    }

    public synchronized boolean addClient(String name, ClientHandlerInterface clientHandler) {
        if (!activeUsers.containsKey(name)) {
            activeUsers.put(name, clientHandler);
            PropertyChangeListener changeListener = propertyChangeEvent -> {
                if (propertyChangeEvent.getPropertyName().equals(ListenerMap.PROP_PUT)) {
                    String tmp = (String) propertyChangeEvent.getNewValue();
                    clientHandler.addUser(tmp, activeUsers.get(tmp));
                }
                if (propertyChangeEvent.getPropertyName().equals(ListenerMap.PROP_REMOVE)) {
                    if (propertyChangeEvent.getOldValue().equals(name)) {
                        activeUsers.removePropertyChangeListener(listeners.get(name));
                        listeners.remove(name);
                    } else {
                        clientHandler.removeUser((String) propertyChangeEvent.getOldValue());
                    }
                }
            };
            listeners.put(name, changeListener);
            activeUsers.addPropertyChangeListener(changeListener);
            System.out.println(name + " entered chat :D");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public synchronized void removeClient(String name) {
        activeUsers.remove(name);
    }

    public boolean hasClient(String name) {
        return activeUsers.containsKey(name);
    }

    public Map<String, ClientHandlerInterface> getActiveUsers() {
        return new HashMap<>(activeUsers);
    }

    @Override
    public synchronized void addToSocketNum(int val) {
        openedSocketCount += val;
        System.out.println("Current number of sockets:" + openedSocketCount);
    }

    private void mainLoop() {
        Socket socket;

        while (true)
        {
            try {
                socket = serverSocket.accept();
                System.out.println("New client request received : " + socket);
                if (activeUsers.size() <= maxNumOfClients && openedSocketCount < maxNumOfOpenedSockets) {
                    addToSocketNum(1);
                    try {
                        ClientHandler clientHandler = new ClientHandler(socket, this);

                        Thread thread = new Thread(clientHandler);
                        thread.setDaemon(true);
                        thread.start();
                    } catch (IOException exception) {
                        addToSocketNum(-1);
                    }
                } else {
                    System.out.println("Too many clients :(");
                    DataOutputStream stream = new DataOutputStream(socket.getOutputStream());
                    stream.writeUTF(Config.TOO_MANY_CLIENTS_MESSAGE);
                }
            } catch (IOException exception) {
                System.out.println("Binding socket unsuccessful");
                exception.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        mainLoop();
    }
}