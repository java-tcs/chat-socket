package classes;

import interfaces.ClientHandlerInterface;
import interfaces.ServerInterface;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

class ClientHandler implements Runnable, ClientHandlerInterface
{
    private Map<String, ClientHandlerInterface> activeUsers;
    private String userName;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private Socket socket;
    private ServerInterface server;

    ClientHandler(Socket socket, ServerInterface server) throws IOException {
        this.socket = socket;
        this.dataInputStream = new DataInputStream(socket.getInputStream());
        this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
        this.activeUsers = Collections.synchronizedMap(new HashMap<>());
        this.server = server;
    }

    @Override
    public void addUser(String userName, ClientHandlerInterface clientHandler) {
        if (!activeUsers.containsKey(userName)) {
            activeUsers.put(userName, clientHandler);
            try {
                dataOutputStream.writeUTF(Config.ADD_USER_HEADER + userName + "\0");
            } catch (IOException ignored) {}
        }
    }

    @Override
    public void removeUser(String userName) {
        if (activeUsers.containsKey(userName)) {
            activeUsers.remove(userName);
            try {
                dataOutputStream.writeUTF(Config.REMOVE_USER_HEADER + userName + "\0");
            } catch (IOException ignored) {}
        }
    }

    public synchronized boolean sendMessage(String from, String to, String message) {
        try {
            dataOutputStream.writeUTF(Config.NORMAL_MESSAGE_HEADER + from + "\0" + to + "\0" + message);
            return true;
        } catch (IOException exception) {
            return false;
        }
    }

    private void handleNormalMessage(StringTokenizer stringTokenizer) throws IOException{
        String receiver = stringTokenizer.nextToken();
        String message = stringTokenizer.nextToken();
        if (receiver.equals("All")) {
            for (ClientHandlerInterface handler : activeUsers.values()) {
                if (handler != null) {
                    handler.sendMessage(userName , receiver, message);
                }
            }
        } else {
            ClientHandlerInterface receiverHandler = activeUsers.get(receiver);
            if (receiverHandler != null
                    && receiverHandler.sendMessage(userName, receiver, message)) {
                if (receiverHandler != this) {
                    dataOutputStream.writeUTF(Config.NORMAL_MESSAGE_HEADER + userName + "\0" + receiver + "\0" + message);
                }
            } else {
                dataOutputStream.writeUTF(Config.NO_SUCH_USER_MESSAGE);
            }
        }
    }

    private boolean establishName() {
        boolean nameEstablished = false;
        try {
            String message;
            for (int i = 0; i < 10 && !nameEstablished; i++) {
                message = dataInputStream.readUTF();
                if (Config.USERNAME_REGEX.matcher(message).matches()) {
                    int usernameEnd = message.substring(Config.USER_NAME_REQUEST.length()).indexOf('\0');
                    message = message.substring(Config.USER_NAME_REQUEST.length(),
                            Config.USER_NAME_REQUEST.length() + usernameEnd);
                    this.userName = message;
                    if (!activeUsers.containsKey(message) && server.addClient(message,this)) {
                        nameEstablished = true;
                        dataOutputStream.writeUTF(Config.USER_NAME_REQUEST_APPROVAL);
                        server.getActiveUsers().forEach(this::addUser);
                    } else {
                        i = -1;
                        System.out.println("Given username (" + message + ") already taken");
                        dataOutputStream.writeUTF(Config.USERNAME_TAKEN_MESSAGE);
                    }
                } else {
                    i = -1;
                    System.out.println("Given username in wrong format");
                    dataOutputStream.writeUTF(Config.WRONG_FORMAT_MESSAGE);
                }
            }
        } catch (IOException ignored) {

        }
        return nameEstablished;
    }

    private void onEndOfCommunication() {
        server.removeClient(userName);
        server.addToSocketNum(-1);
        System.out.println(userName + " left chat :(");
    }

    @Override
    public void run() {
        if (!establishName()) {
            System.out.println("Couldn't establish name");
            server.addToSocketNum(-1);
            return;
        }

        String receivedMessage;

        while (true) {
            try {
                receivedMessage = dataInputStream.readUTF();
                StringTokenizer stringTokenizer = new StringTokenizer(receivedMessage, "\0");
                if (stringTokenizer.countTokens() >= 4
                        && stringTokenizer.nextToken().equals(Config.NORMAL_MESSAGE_HEADER.substring(0, Config.NORMAL_MESSAGE_HEADER.length()-1))
                        && stringTokenizer.nextToken().equals(userName)) {
                    handleNormalMessage(stringTokenizer);
                } else {
                    dataOutputStream.writeUTF(Config.WRONG_FORMAT_MESSAGE);
                }
            } catch (IOException exception) {
                onEndOfCommunication();
                return;
            }
        }
    }
} 