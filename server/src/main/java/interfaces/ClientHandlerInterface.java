package interfaces;

public interface ClientHandlerInterface {
    boolean sendMessage(String from, String to, String message);
    void addUser(String userName, ClientHandlerInterface clientHandler);
    void removeUser(String userName);
}
