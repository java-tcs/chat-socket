package interfaces;

import java.util.Map;

public interface ServerInterface {
    boolean addClient(String name, ClientHandlerInterface clientHandler);
    void removeClient(String name);
    boolean hasClient(String name);
    Map<String, ClientHandlerInterface> getActiveUsers();
    void addToSocketNum(int val);
}
