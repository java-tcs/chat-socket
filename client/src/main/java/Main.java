import controller.Controller;
import interfaces.ControllerInterface;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class Main extends Application {
    private final static int serverPort = 1234;

    @Override
    public void start(Stage primaryStage) throws Exception {
        ControllerInterface controller = new Controller(primaryStage, serverPort);
        Platform.runLater(controller);
    }


    public static void main(String[] args) {
        launch(args);
    }

}
