package interfaces;

public interface ControllerInterface extends Runnable{
    String askForName(String message);
    String askForHost(String message);
    void addUser(String userName);
    void setUserPresent(String userName);
    void setUserAbsent(String userName);
    void updateConversation(String userName, String value);
    void setUserName(String userName);
    void onLostConnection();
}
