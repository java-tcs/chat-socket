package interfaces;

public interface ModelInterface extends Runnable {
    void sendMessage(String name, String message);
    String getConversation(String name);
    void close();
}
