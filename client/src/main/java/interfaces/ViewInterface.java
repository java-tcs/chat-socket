package interfaces;

import javafx.beans.value.ChangeListener;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;

public interface ViewInterface {
    void show();
    String askForName(String message);
    String askForHost(String message);
    TextArea getInputTextArea();
    TextArea getChatTextArea();
    ListView getMyListView();
    void setUserAbsent(String userName);
    void addUser(String userName);
    void setUserPresent(String userName);
    void addListViewListener(ChangeListener listener);
    Text getText();
    void setAppTitle(String title);
    Button getSendButton();
    Button getHostButton();
    Button getNameButton();
    void clearAll();
}
