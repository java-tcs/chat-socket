package model;

import java.util.regex.Pattern;

class Config {
    final static String OPT_HEADER = "01\0";
    final static String USER_NAME_REQUEST = OPT_HEADER + "01\0";
    final static String ADD_USER_HEADER = OPT_HEADER + "02\0";
    final static String REMOVE_USER_HEADER = OPT_HEADER + "03\0";
    final static String USER_NAME_REQUEST_APPROVAL = USER_NAME_REQUEST + "01\0";

    final static String NORMAL_MESSAGE_HEADER = "02\0";

    final static String ERROR_HEADER = "06\0";
    final static String USERNAME_TAKEN_MESSAGE = ERROR_HEADER + "01\0";
    final static String COULDNT_SEND_MESSAGE = ERROR_HEADER + "02\0";
    final static String WRONG_FORMAT_MESSAGE = ERROR_HEADER + "03\0";
    final static String NO_SUCH_USER_MESSAGE = ERROR_HEADER + "04\0";
    final static String TOO_MANY_CLIENTS_MESSAGE = ERROR_HEADER + "05\0";
    final static String USERNAME_REGEX_STRING = "[A-Za-z0-9_. ]{0,20}";
    final static Pattern USERNAME_REGEX = Pattern.compile(USER_NAME_REQUEST + "[A-Za-z0-9_. ]{0,20}" + "\0");
}
