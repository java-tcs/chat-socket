package model;

import interfaces.ControllerInterface;
import interfaces.ModelInterface;
import javafx.application.Platform;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Random;
import java.util.StringTokenizer;

public class Model implements ModelInterface {
    private ControllerInterface controller;
    private String[] nullUsernameMessages = {"Could You please enter username?", "You give username me thank you", "Please enter username"};
    private Socket socket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private String username;
    private HashMap<String, String> conversations;
    private boolean close;

    public Model(ControllerInterface controllerInterface, Socket socket) {
        this.controller = controllerInterface;
        this.conversations = new HashMap<>();
        close = false;
        this.socket = socket;
        try {
            this.dataInputStream = new DataInputStream(socket.getInputStream());
            this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
        } catch (IOException exception) {
            exception.printStackTrace();
            Platform.runLater(() -> controller.onLostConnection());
        }
    }

    public String getConversation(String name) {
        return conversations.get(name);
    }

    @Override
    public void close() {
        close = true;
        try {
            socket.close();
        } catch (IOException ignored) {}
    }

    private String sendAndReceive(String message) throws IOException {
        String result = null;
        socket.setSoTimeout(2000);
        for (int i = 0; i < 10 && result == null; i++) {
            dataOutputStream.writeUTF(message);
            try {
                result = dataInputStream.readUTF();
            } catch (SocketTimeoutException exception) {
                System.out.println("Timeout");
            }
        }
        socket.setSoTimeout(0);
        return result;
    }

    private boolean establishName() {
        Random random = new Random();
        String popupMessage = nullUsernameMessages[random.nextInt(nullUsernameMessages.length)];
        try {
            while (true) {
                username = controller.askForName(popupMessage);
                if (username != null) {
                    String result = sendAndReceive(Config.USER_NAME_REQUEST + username + "\0");
                    if (result.equals(Config.USER_NAME_REQUEST_APPROVAL)) {
                        controller.setUserName(username);
                        return true;
                    } else {
                        if (result.equals(Config.USERNAME_TAKEN_MESSAGE)) {
                            popupMessage = "Username already taken";
                            continue;
                        }
                        if (result.equals(Config.WRONG_FORMAT_MESSAGE)) {
                            popupMessage = "Incorrect username format \n(please match this regular expresison: " + Config.USERNAME_REGEX_STRING + ")";
                            continue;
                        }
                        if (result.equals(Config.TOO_MANY_CLIENTS_MESSAGE)) {
                            popupMessage = "Sorry, there is too many users logged in :(";
                            controller.askForName(popupMessage);
                        }
                        if (!close) {
                            Platform.runLater(() -> controller.onLostConnection());
                        }
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } catch (IOException exception) {
            if (!close) {
                Platform.runLater(() -> controller.onLostConnection());
            }
            return false;
        }
    }

    private void handleNormalMessage(String msg) {
        StringTokenizer stringTokenizer = new StringTokenizer(msg, "\0");
        if (stringTokenizer.countTokens() >= 4) {
            stringTokenizer.nextToken();
            String from = stringTokenizer.nextToken();
            String to = stringTokenizer.nextToken();
            String message = stringTokenizer.nextToken();
            if (to.equals("All")) {
                conversations.put("All", conversations.get("All") + from + ": " + message + "\n");
                Platform.runLater(() -> controller.updateConversation("All", conversations.get("All")));
            } else {
                if (from.equals(username)) {
                    conversations.put(to, conversations.get(to) + from + ": " + message + "\n");
                    Platform.runLater(() -> controller.updateConversation(to, conversations.get(to)));
                } else {
                    conversations.put(from, conversations.get(from) + from + ": " + message + "\n");
                    Platform.runLater(() -> controller.updateConversation(from, conversations.get(from)));
                }
            }
        }
    }



    @Override
    public void run() {
        if (!establishName()) {
            return;
        }

        Thread readMessage = new Thread(() -> {
            while (true) {
                if (close) {
                    return;
                }
                try {
                    String msg = dataInputStream.readUTF();
                    if (msg.indexOf(Config.ADD_USER_HEADER) == 0) {
                        msg = msg.substring(Config.ADD_USER_HEADER.length());
                        final String name = msg.substring(0, msg.indexOf("\0"));
                        if (!conversations.containsKey(name)) {
                            conversations.put(name, "");
                            Platform.runLater(() -> controller.addUser(name));
                        } else {
                            Platform.runLater(() -> controller.setUserPresent(name));
                        }
                    }
                    if (msg.indexOf(Config.REMOVE_USER_HEADER) == 0) {
                        msg = msg.substring(Config.ADD_USER_HEADER.length());
                        final String name = msg.substring(0, msg.indexOf("\0"));
                        Platform.runLater(() -> controller.setUserAbsent(name));
                    }
                    if (msg.indexOf(Config.NORMAL_MESSAGE_HEADER) == 0) {
                        handleNormalMessage(msg);
                    }
                } catch (IOException e) {
                    if (!close) {
                        Platform.runLater(() -> controller.onLostConnection());
                    }
                    return;
                }
            }
        });

        readMessage.setDaemon(true);
        readMessage.start();
    }

    @Override
    public void sendMessage(String name, String message) {
        try {
            dataOutputStream.writeUTF(Config.NORMAL_MESSAGE_HEADER + username + "\0" + name + "\0" + message);
        } catch (IOException exception) {
            if (!close) {
                Platform.runLater(() -> controller.onLostConnection());
            }
        }
    }
}
