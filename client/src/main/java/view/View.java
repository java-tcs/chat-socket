package view;


import interfaces.ViewInterface;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class View implements ViewInterface {
    private ObservableList<String> list;
    private ListProperty<String> listProperty = new SimpleListProperty<>();
    private TextArea inputTextArea;
    private TextArea chatTextArea;
    private Stage primaryStage;
    private ListView myListView;
    private Text userName;
    private TextInputDialog usernameDialog, hostDialog;
    private Button sendButton, hostButton, nameButton;
    private int maxCharInTextArea = 1024;

    public View(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/client.fxml"));
        primaryStage.setTitle("Chat");
        primaryStage.setScene(new Scene(root));
        this.primaryStage = primaryStage;
        this.myListView = (ListView) root.lookup("#list");
        this.userName = (Text) root.lookup("#userName");
        this.inputTextArea = (TextArea) root.lookup("#inputTextField");
        this.chatTextArea = (TextArea) root.lookup("#chatTextArea");
        this.sendButton = (Button) root.lookup("#sendButton");
        this.hostButton = (Button) root.lookup("#hostButton");
        this.nameButton = (Button) root.lookup("#nameButton");

        usernameDialog = new TextInputDialog("Jane Doe");
        hostDialog = new TextInputDialog("localhost");

        myListView.itemsProperty().bind(listProperty);

        this.list = FXCollections.observableArrayList(new ArrayList<>());
        listProperty.set(list);
        sendButton.setMouseTransparent(true);
        inputTextArea.setMouseTransparent(true);
        inputTextArea.setTextFormatter(new TextFormatter<String>(change ->
                change.getControlNewText().length() <= maxCharInTextArea ? change : null));
    }

    public void addListViewListener(ChangeListener listener) {
        myListView.getSelectionModel().selectedItemProperty().addListener(listener);
    }

    public void addListItem(String name) {
        list.add(name);
    }

    @Override
    public void show() {
        primaryStage.show();
    }

    @Override
    public TextArea getInputTextArea() {
        return inputTextArea;
    }

    @Override
    public TextArea getChatTextArea() {
        return chatTextArea;
    }

    @Override
    public ListView getMyListView() {
        return myListView;
    }

    @Override
    public Text getText() {
        return userName;
    }

    @Override
    public Button getSendButton() {
        return sendButton;
    }

    @Override
    public Button getHostButton() {
        return hostButton;
    }

    @Override
    public Button getNameButton() {
        return nameButton;
    }

    @Override
    public void clearAll() {
        this.inputTextArea.clear();
        this.chatTextArea.clear();
        this.userName.setText("");
        this.primaryStage.setTitle("Chat");
        this.myListView.getItems().clear();
        sendButton.setMouseTransparent(true);
        inputTextArea.setMouseTransparent(true);
    }

    @Override
    public void setAppTitle(String title) {
        primaryStage.setTitle(title);
    }

    @Override
    public void setUserAbsent(String userName) {
        myListView.getItems().forEach((item) -> {
            if (item instanceof Text && ((Text) item).getText().equals(userName)) {
                ((Text) item).setFill(Color.RED);
            }
        });
    }

    @Override
    public void addUser(String userName) {
        myListView.getItems().add(new Text(userName));
    }

    @Override
    public void setUserPresent(String userName) {
        myListView.getItems().forEach((item) -> {
            if (item instanceof Text && ((Text) item).getText().equals(userName)) {
                ((Text) item).setFill(Color.BLACK);
            }
        });
    }

    @Override
    public String askForName(String message) {
        usernameDialog.setTitle("");
        usernameDialog.setHeaderText(message);
        usernameDialog.setContentText("Username:");

        Optional<String> result = usernameDialog.showAndWait();

        return result.orElse(null);
    }

    @Override
    public String askForHost(String message) {
        hostDialog.setTitle("");
        hostDialog.setHeaderText(message);
        hostDialog.setContentText("Host: ");

        Optional<String> result = hostDialog.showAndWait();

        return result.orElse(null);
    }
}
