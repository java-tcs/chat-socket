package controller;

import interfaces.ControllerInterface;
import interfaces.ModelInterface;
import interfaces.ViewInterface;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Model;
import view.View;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Controller implements ControllerInterface {
    private int serverPort;
    private ViewInterface view;
    private ModelInterface model;
    private String currentUserName;
    private Socket socket;

    public Controller(Stage primaryStage, int port) throws IOException {
        this.serverPort = port;
        this.view = new View(primaryStage);
        this.socket = null;
    }

    private void setViewHandlers() {
        view.getInputTextArea().setOnKeyReleased(keyEvent -> {
            if (keyEvent.isShiftDown() && keyEvent.getCode() == KeyCode.ENTER) {
                model.sendMessage(currentUserName,
                        view.getInputTextArea().getText());
                view.getInputTextArea().clear();
                view.getInputTextArea().requestFocus();
            }
        });

        view.addListViewListener((observableValue, o, t1) -> {
            if (t1 != null && !t1.equals(o)) {
                currentUserName = ((Text) t1).getText();
                if (((Text) t1).getFill().equals(Color.GREEN)) {
                    ((Text) t1).setFill(Color.BLACK);
                }
                String text = model.getConversation(currentUserName);
                view.getChatTextArea().setText(text);
            }
        });

        view.addListViewListener((observableValue, o, t1) -> {
            if (t1 != null) {
                view.getText().setText(((Text) t1).getText());
                Paint color = ((Text) t1).getFill();
                if (color.equals(Color.GREEN)) {
                    view.getText().setFill(Color.BLACK);
                } else {
                    view.getText().setFill(color);
                }
            } else {
                view.getText().setText("");
            }
        });

        view.getSendButton().setOnMouseClicked(mouseEvent -> {
            model.sendMessage(currentUserName,
                    view.getInputTextArea().getText());
            view.getInputTextArea().clear();
            view.getInputTextArea().requestFocus();
        });

        view.getNameButton().setOnMouseClicked(mouseEvent -> Platform.runLater(model));

        view.getHostButton().setOnMouseClicked(mouseEvent -> connectToServer("Please enter host"));

        view.addListViewListener((observableValue, o, t1) -> {
            view.getInputTextArea().setMouseTransparent(false);
            view.getSendButton().setMouseTransparent(false);
        });
    }

    @Override
    public void run() { 
        setViewHandlers();
        view.show();
        Platform.runLater(() -> connectToServer("Please enter host"));
    }

    private void connectToServer(String message) {
        String popupMessage = message;
        Socket newSocket = new Socket();
        while (!newSocket.isConnected()) {
            try {
                String hostString = askForHost(popupMessage);
                if (hostString != null) {
                    newSocket.connect(new InetSocketAddress(hostString, serverPort), 2000);
                    if (model != null) {
                        model.close();
                    }
                    view.clearAll();
                    view.getNameButton().setMouseTransparent(false);
                    model = new Model(this, newSocket);
                    socket = newSocket;
                    Platform.runLater(model);
                } else {
                    break;
                }
            } catch (IOException exception) {
                popupMessage = "Couldn't connect to server\nEnter host again";
                try {
                    newSocket.close();
                } catch (IOException ignored) {}
                newSocket = new Socket();
            }
        }
    }

    @Override
    public String askForName(String message) {
        return view.askForName(message);
    }

    @Override
    public String askForHost(String message) {
        return view.askForHost(message);
    }

    @Override
    public void setUserPresent(String userName) {
        if (userName.equals(currentUserName)) {
            view.getText().setFill(Color.BLACK);
        }
        view.setUserPresent(userName);
    }

    @Override
    public void setUserAbsent(String userName) {
        if (userName.equals(currentUserName)) {
            view.getText().setFill(Color.RED);
        }

        view.setUserAbsent(userName);
    }

    @Override
    public void addUser(String userName) {
        view.addUser(userName);
    }

    @Override
    public void updateConversation(String userName, String value) {
        if (this.currentUserName != null && this.currentUserName.equals(userName)) {
            double scrollValue = view.getChatTextArea().getScrollTop();
            view.getChatTextArea().setText(value);
            view.getChatTextArea().setScrollTop(scrollValue);
        } else {
            view.getMyListView().getItems().forEach(item -> {
                if (((Text)item).getText().equals(userName)) {
                    ((Text)item).setFill(Color.GREEN);
                }
            });
        }
    }

    @Override
    public void setUserName(String userName) {
        view.setAppTitle("Chat: " + userName);
        view.getNameButton().setMouseTransparent(true);
    }

    @Override
    public void onLostConnection() {
        model.close();
        connectToServer("Connection lost :(\nEnter host again");
    }
}
